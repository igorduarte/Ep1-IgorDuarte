#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"

#include <string>

using namespace std;

class Negativo : public Filtro{
	private:
		
	public:
		Negativo();

		void aplicarNegativo(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, char *diretorio);
};

#endif
