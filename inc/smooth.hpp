#ifndef SMOOTH_H
#define SMOOTH_H

#include "filtro.hpp"

#include <string>

using namespace std;

class Smooth : public Filtro{
	private:

	public:
		Smooth();

		void aplicarSmooth(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, int *filter, int size, int div, char *diretorio);
};

#endif
