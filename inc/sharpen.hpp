#ifndef SHARPEN_H
#define SHARPEN_H

#include "filtro.hpp"

#include <string>

using namespace std;

class Sharpen : public Filtro{
	private:

	public:
		Sharpen();
		void aplicarSharpen(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, int *filter, int size, int div, char *diretorio);
};

#endif
